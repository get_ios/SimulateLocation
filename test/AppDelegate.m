//
//  AppDelegate.m
//  test
//
//  Created by 贾仕琪 on 2017/1/22.
//  Copyright © 2017年 贾仕琪. All rights reserved.
//

#import "AppDelegate.h"
#import "WGS84TOGCJ02.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

//http://lbs.amap.com/console/show/picker

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//南裕小区WGS84    29.808143 121.550869 (GCJ02 29.805553, 121.55502)
//恒达高WGS84    29.806756 121.546771 (GCJ02 121.550932,29.804175)

//companyWGS84 lat:29.808984,lng:121.551463  (GCJ02 29.806393, 121.555613)
//companyWGS84 lat:29.808384,lng:121.551696  (GCJ02 29.805793, 121.555845)
//companyWGS84 lat:29.809050,lng:121.551282  (GCJ02 29.806459, 121.555432)

    Location GCJ02 = LocationMake(29.806459, 121.555432);//高德 to WGS
    Location WGS84 = transformFromGCJToWGS(GCJ02);
    NSLog(@"lat:%f,lng:%f",WGS84.lat,WGS84.lng);
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
